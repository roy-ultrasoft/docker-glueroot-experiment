# Glowroot Docker Test
### Start DockerEnv
```bash
docker-compose up --build
```
**URL to Dummy App**  
``http://localhost:8080/experiment-lazy-filtering/``  
**URL to Glowroot**  
``http://localhost:4040/``
***
## Manual Installation [Glowroot-Embedded](https://github.com/glowroot/glowroot/wiki/Agent-Installation-%28with-Embedded-Collector%29) (already done on docker)
**Dowsnload the java-agent**  
- Download the glowroot-${VERSION}-dist.zip [glowroot/releases](https://github.com/glowroot/glowroot/releases)

**Configure the java-agent**  
- unzip  
- add the **admin.json** file in the same directory as glowroot.jar to allow acces from any hosts (default host: localhost, default port: 4000)
```json
{
  "web": {
    "bindAddress": "0.0.0.0",
    "port": "4040"
  }
}
```

### set the JAVA_OPTS  
Add ``-javaagent:path/to/glowroot.jar`` to your application's JVM args.  
see here for details: [Where-are-my-application-server's-JVM-args](https://github.com/glowroot/glowroot/wiki/Where-are-my-application-server's-JVM-args%3F)

**Dockerfile**  
```dockerfile
FROM tomcat
ENV JAVA_OPTS="-javaagent:/opt/glowroot/glowroot.jar"
COPY glowroot/ /opt/glowroot/
COPY webapps/ /usr/local/tomcat/webapps/
COPY conf/ /usr/local/tomcat/conf/
COPY lib/ /usr/local/tomcat/lib/
```
